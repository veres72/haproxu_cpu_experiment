# Health check и как с этим жить не надо

## Health check - зачем? 

А все очень просто - если мы используем какую-то балансировку нагрузки, а мы ее конечно же используем, мы хотим быть уверенны, что наш трафик попадает в живые бекенды. Ведь бекенды умирают (привет OOM) или перезапускаются для установки обновлений.

## Health check - как? 

1. TCP
2. HTTP
3. External check

Опустим унылые объяснения чем отличается TCP check от HTTP и прочее. Важно то, что люди часто увлекаются и заходят слишком далеко в попытке сделать лучше и получается как всегда. И вместо того, что бы удовлетворится простым кодом 200 в ответ на ручку от бекенда, начинают изобретать сложные проверки с nc, curl, grep, многоуровневые if.

А реализовать такую логику просто конфигами нереально. Но слава богу в некоторых LB можно использовать так называемые external check - вызвать любой, какой угодно скрипт, и основываясь на его коде возврата, принять решение - жив бекенд или нет.

Например, вот так:

```bash
#!/bin/bash

APP_CHECK_URL="http://${HAPROXY_SERVER_ADDR}:9099/api/test/state"

NC_TIMEOUT=1
CURL_TIMEOUT=1
CURL_MAX_TIME=1

if [[ "$(curl -s --connect-timeout ${CURL_TIMEOUT} --max-time ${CURL_MAX_TIME} --write-out %{http_code} --output /dev/null ${APP_CHECK_URL} 2>/dev/null)" == 200 ]]; then
    exit 0
fi
exit 1
```

## External check - подводные камни

А чо такого? Годный скрипт жи.
Есть 2 крохотных недостатка данного метода:

### Fork fork fork

Запуск каждого скрипта, даже если он усраться какой оптимальный, и вообще не скрипт, а бинарь на сях, написанный так, что Страуструп взволнованно звонил вам среди ночи и просил поделиться опытом, это все равно запуск отдельного процесса. Это форк. А форк это дорого. Все же знают почему. 

И все бы ничего, пока у вас 10-15 бекендов которые надо иногда проверять. Грустно становится когда их десятки. И каждый надо проверить. И на каждую проверку надо сделать форк, запустить процесс, выделить память, слайc, pid и все такое. 

### Где ресурсы, Лебовски? 

Все скрипты для external check вызываются не в рантайм LB (по крайней мере это справедливо для haproxy) - вы никогда не увидите реальное потребление ресурсов вашим LB, никогда не сможете построить адекватный профиль нагрузки из perf, zabbix, prometheus или еще чего угодно. 

А скрипт наш, как мы помним, написан идеально, и отрабатывает ооооочень быстро. И каждый скрипт - это отдельный процесс, со своим pid. А всякие мониторинги, топы и прочие, собирают данные по загрузке раз в n секунд. И десятки запусков крохотного идеального скрипта они просто пропускают. И получается очень интересная картина - ресурсы сожраны. А кем - хз.

(тут слегка видно высокий LA - но это тестовая вм, на которой проблема искусственно усилена, в проде все не так очевидно конечно же)

Например вот так:

![htop](images/hotp.png)

Или так:

![cpu-util](images/cpu-util.png)

Присмотритесь: 
Процессор утилизирован на 90%. Из них 3 - haproxy, 1 - java, 1 - bash - 2 htop.

Где остальные 80%? Чем заняты?
Как раз нашим идеальным скриптом.

Вот только мы его не увидим. Слишком быстрый скрипт и слишком редкий опрос от любой системы мониторинга. Но у нас есть же профилировщик, чо нет то.
Запускает perf на 10 секунд и наслаждаемся.

![flame_all](images/flame_all.png)

Ну а после всего увиденного, идём в хапрокси и выкидываем на мороз вот эти 3 строчки

```
        option external-check
        external-check path "/usr/bin:/bin:/vagrant/scripts"
        external-check command ext_check.sh
```

И повторяем эксперимент:

![cpu_util](images/cpu_without_checks.png)

Как то так.

Ну и на закусочку картинка с реального прода:

![prod](images/prod.png)

## Вывод? 

- Не делай форк там где не надо;
- Не вызывай внешние скрипты, это дорого, сложно в дебаге, некрасиво;
- Не усложняй себе жизнь, не создавай highload там, где его нет;

# Health check and how to do it wrong. 
## Why we should use health check? 
It's easy. If we use load balancing, we will use a health check, because we want to know our services state. We want to direct user traffic to live services. Sometimes services are dead, sometimes OOM kills services, sometimes service is down for the update and etc. 
## How can we do a health check? 
1. TCP
2. HTTP 
2. External check. 
Let's stop on the external check. 
Sometimes people get carried away and start to use very strong and heavy check. Try to check business logic in external checks and use for example bash scripts. Instead of using just 200 code for check services state, they take a difficult script with grep, for, awk. if else statement and etc. 
All of this, not a problem in monolith architecture or in load balancer with low load. But in the situation, when a lot of services check each other every several seconds, external check become a bad way. 
Every external check is a new fork. The fork is very expensive operations (Hi Postgresql, hi Apache) and in the case with LB, LB doesn't watch on check's script, because of all external script run outside runtime.  And we see a very strange graph in our monitoring system. CPU utilization very hight, but we don't see who uses our resources. 
If collect debugs info through perf the situation becomes more obvious. A lot of CPU time is used for our script, which works very fast, but a lot of this script makes a lot of CPU utilization. 
